						    
                                                        	PROTEINAS          
							   -------------------

La creación de este proyecto esta enfocada en el manejo de clases e ingreso de datos, se busca crear proteínas a las cuales se les debe agregar una cadena, una aminoácido, un átomo y las coordenas (x, y,z).

+ Empezando
    El programa cosiste en un generador de proteínas que inicialmente presenta un menú con 2 opciones. La primera opción es ingresar e imprimir los datos de la proteina, primero se pedira el número de proteínas que se desea, luego se pedira su id y nombre, depués se ingresaran los datos de la cadena, del áminoácido, del átomo y finalmente las coordenadas; una vez ingresado los datos estos seran impresos por la funcion imprimir_datos_proteina(). Finalmente la segunda opción le permite al usuario cerrar el programa.


+ Requisitos previos
    Sistema operativo linux.


+ Ejecutando las pruebas por terminal
    Para la entrada a los archivos en el editor de texto vim, se necesita del comando: 
        [1] vim nombrearchivo.h para archivos .h donde se encuemtra la definición de la clase.
	[2] vim nombrearchivo.cpp para archivos .cpp donde esta la implementación de la clase.
	
    Para compilar el programa se utiliza el comando:
	[3] make

    Mientras que para ejecutar debemos colocar:
        [4] ./programa
        

+ Construido con
    Sistema operativo: Ubuntu 18.04.3 LTS.
    Vim: Editor de texto para escribir el código del programa. 
    C++: Lenguaje de programación imperativo orientado a objetos derivado del lenguaje C.
    Pep-8: La narración del código realizado en este proyecto esta basado en las instrucciones dadas por la pep-8.

+ Versiones
    Ubuntu 18.04.3 LTS.
    c++ (Ubuntu 7.4.0-1ubuntu1~18.04.1) 7.4.0

+ Autor
    Nicole Soto.




