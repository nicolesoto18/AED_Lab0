#include <list>
#include <iostream>
#include "Cadena.h"

using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class Proteina{
    private:
        string nombre = "\0";
        string id = "\0";
        list<Cadena> cadenas;

    public:
        /*Constructor*/
        Proteina(string id, string nombre);

        /*Metodos*/
        void add_cadena(Cadena cadena);
        void set_nombre(string nombre);
        void set_id(string id);
        string get_id();
        string get_nombre();
        list <Cadena> get_cadenas();
};
#endif
        
        
        
        
