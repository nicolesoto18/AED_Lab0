#include <list>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "Proteina.h"
#include "Cadena.h"
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

void imprimir_datos_proteina(Proteina proteina){
    cout << "\n\t Datos [" << proteina.get_nombre() << "]" << endl;
    cout << "ID    : " << proteina.get_id() << endl;   
    cout << "Nombre: " << proteina.get_nombre() << endl;

    for (Cadena cadena: proteina.get_cadenas()){
        cout << "Cadena: " << cadena.get_letra() << endl;
        
        for(Aminoacido aminoacido: cadena.get_aminoacidos()){
            cout << "\n\t Aminoácidos" << endl;
            cout << "Nombre: " << aminoacido.get_nombre() << endl;
            cout << "Número: " << aminoacido.get_numero() << endl;
        
            for (Atomo atomo: aminoacido.get_atomos()){
                cout << "\n\t Átomos" << endl;
                cout << "Nombre: " << atomo.get_nombre() << endl;
                cout << "Número: " << atomo.get_numero() << endl;

                cout << "\n\t Coordenadas" << endl;
                cout << "X     : " << atomo.get_coordenada().get_x() << endl; 
                cout << "Y     : " << atomo.get_coordenada().get_y() << endl;
                cout << "Z     : " << atomo.get_coordenada().get_z() << endl;
            }
        }
    }
}


void leer_datos_proteina(){
    list<Proteina> proteinas;
    string nom_proteina;
    string nom_cadena;
    string nom_aminoacido;
    string nom_atomo;
    string id;
    string num_aminoacido;
    string num_atomo;
    string coor_x;
    string coor_y;
    string coor_z;
    
    cout << "\n\nIngrese el nombre de la proteína: ";
    getline(cin, nom_proteina);

    cout << "\n\nID de la proteína -> ";
    getline(cin, id);

    cout << "\n\nIngrese una letra para la cadena: ";
    getline(cin, nom_cadena);

    cout << "\nIngrese el nombre del aminoácido: ";
    getline(cin, nom_aminoacido);

    cout << "\nIngrese el número del aminoácido: ";
    getline(cin, num_aminoacido);

    cout << "\nIngrese el nombre del átomo: ";
    getline(cin, nom_atomo);

    cout << "\nIngrese el número del átomo: ";
    getline(cin, num_atomo);
    
    cout << "\nIngrese las coordenadas del átomo" << endl;

    cout << "X -> ";
    getline(cin, coor_x);

    cout << "Y -> ";
    getline(cin, coor_y);

    cout << "Z -> ";
    getline(cin, coor_z);


    /* Crear objetos*/ 
    Proteina proteina1 = Proteina(id, nom_proteina);
    Cadena cadena1 = Cadena(nom_cadena);
    Atomo atomo1 = Atomo(nom_atomo, stoi(num_atomo));
    Aminoacido aminoacido1 = Aminoacido(nom_aminoacido, stoi(num_aminoacido));

    Coordenada coordenada1 = Coordenada(stof(coor_x), stof(coor_y), stof(coor_z));
    atomo1.set_coordenada(coordenada1);

   /* Coordenada coordenada1 = Coordenada();
    coordenada1.set_x(stof(coor_x));
    coordenada1.set_y(stof(coor_y));
    coordenada1.set_z(stof(coor_z));*/

    aminoacido1.add_atomo(atomo1);
    cadena1.add_aminoacido(aminoacido1);
    proteina1.add_cadena(cadena1);

    proteinas.push_back(proteina1);


    system("clear");
    imprimir_datos_proteina(proteina1);
}
    
    
int main(int argc, char **argv){
    string opcion;
    string num_cadena;

    srand(time(NULL));
    
    cout << "\t-----------" << endl;
    cout << "\t Proteinas" << endl;
    cout << "\t-----------" << endl;

    cout << " [1] Ingresar e imprimir datos\n [2] Salir\n";   
    getline(cin, opcion);
    system("clear");
    
    if(stoi(opcion) == 1){
        cout << "Ingrese la cantidad de proteinas que se crearan: " << endl;
        getline(cin, num_cadena);
        
        for (int i = 0; i < stoi(num_cadena); i++){
            leer_datos_proteina();
        }
    }

    else if (stoi(opcion) == 2){
        exit(0);
    }

    return 0;
}
    
