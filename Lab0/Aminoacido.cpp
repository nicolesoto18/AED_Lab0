#include <list>
#include <iostream>
#include "Aminoacido.h"
#include "Atomo.h"

using namespace std;

Aminoacido::Aminoacido(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}


void Aminoacido::set_nombre(string nombre){
    this->nombre = nombre;
}


void Aminoacido::set_numero(int numero){
    this->numero = numero;
}


void Aminoacido::add_atomo(Atomo atomo){
    this->atomos.push_back(atomo);
}


string Aminoacido::get_nombre(){
    return this->nombre;
}


int Aminoacido::get_numero(){
    return this->numero;
}


list<Atomo> Aminoacido::get_atomos(){
    return this->atomos;
}


