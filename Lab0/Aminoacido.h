#include <list>
#include <iostream>
#include "Atomo.h"

using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class Aminoacido{
    private:
        string nombre = "\0";
        int numero;
        list<Atomo> atomos;

    public:
        /*Constructor*/
        Aminoacido(string nombre, int numero);
        
        /*Métodos*/
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomo(Atomo atomo);
        string get_nombre();
        int get_numero();
        list <Atomo> get_atomos();

};
#endif
