#include <list>
#include <iostream>
#include "Atomo.h"
#include "Coordenada.h"

using namespace std;

Atomo::Atomo(string nombre, int numero){
    this->nombre = nombre;
    this->numero = numero;
}


void Atomo::set_nombre(string nombre){
    this->nombre = nombre;
}


void Atomo::set_numero(int numero){
    this->numero = numero;
}


void Atomo::set_coordenada(Coordenada coordenada){
    this->coordenada = coordenada;
}

    
int Atomo::get_numero(){
    return this->numero;
}


string Atomo::get_nombre(){
    return this->nombre;
}


Coordenada Atomo::get_coordenada(){
    return this->coordenada;
}

