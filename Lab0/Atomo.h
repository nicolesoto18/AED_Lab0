#include <list>
#include <iostream>
#include "Coordenada.h"

using namespace std;

#ifndef ATOMO_H
#define ATOMO_H

class Atomo{
    private:
        string nombre = "\0";
        Coordenada coordenada;
        int numero;

    public:
        /*Constructor*/
        Atomo(string nombre, int numero);

        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(Coordenada coordenada);
        int get_numero();
        Coordenada get_coordenada();
        string get_nombre();
        

};
#endif
